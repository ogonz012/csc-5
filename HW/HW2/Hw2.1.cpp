/*
* Name:Omar Gonzalez
* Student ID:2496505
* Date:09/13/13
* HW:2
* Problem:1
* I certify this is my own work and code
*/

#include <cstdlib>
/*
 * Forgetting to declare the library <iostream> and cout and cin variables
 */

using namespace std;

int main(int argc, char** argv) {
    
    /* The following code will not work due to the compiling error for not 
     declaring your library.*/
    string name;
    cout<< "Hello my name is Hal, please state your name."<<endl;
    cin>>name;
    cout<<"Thank you, "<<name<<". How is your day going?"<<endl;
    
    /* The following is a logic error*/
    
    int x,y;
    int average;
    cout<< "Enter the numbers for the average"<<endl;
    cin>>x>>y;
    average= x+y/2; /* The average is not actually the average, but instead
                     it is x being added to the outcome of (y/2)*/
    cout<< average<<endl;
    
    
    
    return 0;
}

