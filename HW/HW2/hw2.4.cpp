#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

/*
 * Mad lib
 */
int main() {

    string name;
    string name_two;
    string food;
    string number;
    string color;
    string animal;
    string adj;
    
    cout <<"Please enter a name."<<endl;
    cin>> name;
    cout <<"Please enter another name."<<endl;
    cin>>name_two;
    cout <<"Please enter a food."<<endl;
    cin>>food;
    cout <<"Please enter a number from 100-120."<<endl;
    cin>>number;
    cout <<"Please enter a color"<<endl;
    cin>>color;
    cout <<"Please enter an animal"<<endl;
    cin>>animal;
    cout <<"Please enter an adjective"<<endl;
    cin>>adj;
    
    cout<< "Dear " <<name<<endl;
    
    cout<<"I am sorry that I am unable to turn in my homework at this"<<endl;
    cout<<" time. First, I ate a rotten "<<food<<", which made me"<<color<<endl;
    cout<<" turn and extremely ill. I cam down with a fever of "<<number<<endl;
    cout<<". Next, my "<<adj<<" pet "<<animal<<" must have smelled the "<<endl;
    cout<<"remaining of the "<<food<<" on my homework because he ate it."<<endl;
    cout<<" I am currently rewriting my homework and hope you will"<<endl;
    cout<<" accept it late."<<endl;
    cout<<endl;
    cout<<endl;
    cout<<"Sincerely,"<<endl;
    cout<<name_two<<endl;
    
    return 0;
}
    
    
    
    
    
    
    